	<div id="loading"><div class="cp-spinner cp-round"></div></div>

    <script src="{$settings['rootdir']}/assets/js/moment-with-locales.min.js"></script>
    <script src="{$settings['rootdir']}/assets/js/js.php?v6"></script>
    <script src="{$settings['rootdir']}/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="{$settings['rootdir']}/assets/js/jquery.tablesorter.min.js"></script>
    <script src="{$settings['rootdir']}/assets/js/jquery.tablesorter.widgets.js"></script>

    <script src="{$settings['rootdir']}/assets/js/magic.js?v=6"></script>
    <script src="{$settings['rootdir']}/assets/js/custom.js?v=6"></script>    
  </body>
</html>