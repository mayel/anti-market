# Anti-market README #

"Anti-market" is an app used at the Khora free-shop in Athens (http://wiki.khora.social.coop/)
It is a fork of "Drop App - to manage warehouses that supply clothing and other stuff to refugees in camps." 

### What is this repository for? ###

To enable us to continue to develop this application into something even more beautiful and sustainable. Making it easy for us to work together with a bigger team even if we're not all on the same location.

### How do I get set up? ###

You will not find a lot of documentation. Installing, running and customizing it will be pretty much straight forward for a developer with knowledge of PHP and Mysql.
Create a new database and import market_clean.sql

Then copy .htaccess.default and /lib/config.php.default and remove the .default in the filename. Then change the necesary usernames, folders and database settings.

After this you should be able to login to the app using emailaddress: demo@example.com with password: demo

### Contribution guidelines ###

You gotta be awesome and kind

### Who made this? ###

The Wishing Tree team (wishingtree.eu) originally developed this app for Drop In The Ocean (http://www.drapenihavet.no/en/) 
At this moment they don't actively support or develop the application for others. 

- Maarten
- Martijn
- Bart: post@bartdriessen.eu

### License ###

See the [LICENSE](LICENSE.md) file for license rights and limitations (MIT).